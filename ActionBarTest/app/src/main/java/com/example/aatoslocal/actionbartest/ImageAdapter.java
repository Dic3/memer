package com.example.aatoslocal.actionbartest;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

// Code for starting window

public class ImageAdapter extends BaseAdapter {

    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        ImageView imageView2;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, MainActivity.screenWidth));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
            imageView = (ImageView) convertView;
            }
            imageView2 = imageView;
            imageView2.setImageBitmap(
                decodeSampledBitmapFromResource(mContext.getResources(), mThumbIds[position], 100, 100));
        return imageView;
    }

    public Integer[] mThumbIds = {
            R.drawable.template1, R.drawable.template122,
            R.drawable.template2, R.drawable.template133,
            R.drawable.template3, R.drawable.template144,
            R.drawable.template4, R.drawable.template15,
            R.drawable.template5, R.drawable.template16,
            R.drawable.template6, R.drawable.template177,
            R.drawable.template7, R.drawable.template18,
            R.drawable.template88, R.drawable.template19,
            R.drawable.template9, R.drawable.template20,
            R.drawable.template10, R.drawable.template21,
            R.drawable.template11
    };

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

            final int height = options.outHeight;
            final int width = options.outWidth;
              int inSampleSize = 1;

                  if (height > reqHeight || width > reqWidth) {

                  final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}