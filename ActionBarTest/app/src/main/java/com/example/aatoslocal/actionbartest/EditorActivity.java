package com.example.aatoslocal.actionbartest;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

// Code for handling activities in application's editor view

public class EditorActivity extends AppCompatActivity implements TextInputFragment.TopSectionListener{

    public static ImageView memeImageView;
    public static TextView topTextView;
    public static TextView bottomTextView;
    public static String ts;
    public static Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Editor");

        memeImageView = (ImageView)findViewById(R.id.image_meme);
        topTextView = (TextView)findViewById(R.id.topMemeText);
        bottomTextView = (TextView)findViewById(R.id.bottomMemeText);
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyy-hhmmss");
        ts = s.format(new Date());
        i = getIntent();

        ImageSourceChooser(MainActivity.source);
    }

    public void ImageSourceChooser(int sx){

        if(sx == 0) {
            int position = i.getExtras().getInt("id");
            ImageAdapter imageAdapter = new ImageAdapter(this);
            memeImageView.setImageResource(imageAdapter.mThumbIds[position]);
        } else if(sx == 1){
            PictureFragment.imagePath = MainActivity.selectedImagePath;
            PictureFragment.rawBitmap = (PictureFragment.decodeSampledBitmapFromFile(MainActivity.selectedImagePath, 500, 0));
            PictureFragment.orientedBitmap = ExifUtil.rotateBitmap(PictureFragment.imagePath, PictureFragment.rawBitmap);
            PictureFragment.memeImage.setImageBitmap(PictureFragment.orientedBitmap);
        } else {
            Toast.makeText(this,"source not found",
            Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void createMeme(String top, String bottom) {
        PictureFragment bottomFragment = (PictureFragment) getSupportFragmentManager().findFragmentById(R.id.fragment2);
        bottomFragment.setMemeText(top, bottom);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_save:
                saveImage();
                return true;
            case R.id.action_close:
                finishAffinity();
                System.exit(0);
                default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveImage(){

        memeImageView.buildDrawingCache();
        Bitmap bm=memeImageView.getDrawingCache();
        topTextView.buildDrawingCache();
        Bitmap bm1=topTextView.getDrawingCache();
        bottomTextView.buildDrawingCache();
        Bitmap bm2=bottomTextView.getDrawingCache();

        Bitmap bmap = overlay(bm, bm1, bm2);

        Toast.makeText(this, "Meme saved", Toast.LENGTH_SHORT).show();
        OutputStream fOut = null;
        Uri outputFileUri;
        try {
            File root = new File(Environment.getExternalStorageDirectory() + File.separator + "DCIM" + File.separator
                    + File.separator + "Memer" + File.separator);
            root.mkdirs();
            File sdImageMainDirectory = new File(root, ts+".jpg");
            outputFileUri = Uri.fromFile(sdImageMainDirectory);
            fOut = new FileOutputStream(sdImageMainDirectory);

        } catch (Exception e) {
            Toast.makeText(this, "Error occured. Please try again later.",
                    Toast.LENGTH_SHORT).show();
        }
        try {
            bmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
        }
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ Environment.getExternalStorageDirectory())));
    }

    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2, Bitmap bmp3) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, 0, null);
        canvas.drawBitmap(bmp3, 0, bmp1.getHeight()-bmp3.getHeight(), null);
        return bmOverlay;
    }
}