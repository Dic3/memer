package com.example.aatoslocal.actionbartest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

// Handles the text input events

public class TextInputFragment extends Fragment {

    private static EditText topTextInput;
    private static EditText bottomTextInput;
    private static Button fontButtonTop;
    private static Button fontButtonBottom;
    private static Button clearButton;
    AlertDialog fontDialogTop;
    AlertDialog fontDialogBottom;
    TopSectionListener activityCommander;

    public interface TopSectionListener{
       public void createMeme(String top, String bottom);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            activityCommander = (TopSectionListener) activity;
        }catch(ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text_input, container, false);
        topTextInput = (EditText) view.findViewById(R.id.topTextInput);
        bottomTextInput = (EditText) view.findViewById(R.id.bottomTextInput);
        fontButtonTop = (Button) view.findViewById(R.id.font_button_top);
        fontButtonBottom = (Button) view.findViewById(R.id.font_button_bottom);
        clearButton = (Button) view.findViewById(R.id.button_clear);

        final Button button = (Button) view.findViewById(R.id.button_generate);

        button.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        buttonClicked(v);
                    }
                }


        );

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearText();
            }
        });
        fontButtonTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFontTop();
            }
        });
        fontButtonBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFontBottom();
            }
        });

        return view;
     }

    private void openFontTop(){

        final CharSequence[] items = {" Small ", " Normal ", " Large "," Huge"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Font size");
        builder.setCancelable(true);
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        PictureFragment.topMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);
                        break;
                    case 1:
                        PictureFragment.topMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
                        break;
                    case 2:
                        PictureFragment.topMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 38);
                        break;
                    case 3:
                        PictureFragment.topMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 42);
                        break;
                }
                fontDialogTop.dismiss();
            }
        });
        fontDialogTop = builder.create();
        fontDialogTop.show();
    }
    private void openFontBottom(){

        final CharSequence[] items = {" Small ", " Normal ", " Large "," Huge"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Font size");
        builder.setCancelable(true);
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        PictureFragment.bottomMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);
                        break;
                    case 1:
                        PictureFragment.bottomMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
                        break;
                    case 2:
                        PictureFragment.bottomMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 38);
                        break;
                    case 3:
                        PictureFragment.bottomMemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 42);
                        break;
                }
                fontDialogBottom.dismiss();
            }
        });
        fontDialogBottom = builder.create();
        fontDialogBottom.show();
    }

    public void buttonClicked(View view){
        activityCommander.createMeme(topTextInput.getText().toString(), bottomTextInput.getText().toString());
    }

    public void clearText(){
        topTextInput.setText(null);
        bottomTextInput.setText(null);
        activityCommander.createMeme(null, null);

    }
}



