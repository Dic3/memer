package com.example.aatoslocal.actionbartest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

// Code for handling ActionBar events

public class MainActivity extends AppCompatActivity {

    public static final int SELECT_PICTURE = 1;
    private static final int REQUEST_CODE_CAPTURE_IMAGE_ACTIVITY = 2;
    public static String selectedImagePath;
    public static int screenWidth;
    public static Intent i;
    public static int source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_actionlogo_courier);

        source = 1;

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                i = new Intent(getApplicationContext(), EditorActivity.class);
                i.putExtra("id", position);
                source = 0;
                startActivity(i);
            }
        });

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = (metrics.widthPixels/3)-10;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_gallery:
                openGallery();
                return true;
            case R.id.action_camera:
                openCamera();
                return true;
            case R.id.action_info:
                openInfo();
                return true;
            case R.id.action_close:
                System.exit(0);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_IMAGE_ACTIVITY);

    }
    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                source = 1;
                 Intent intent = new Intent(this, EditorActivity.class);
                    startActivity(intent);

            }
        }
        if (requestCode == REQUEST_CODE_CAPTURE_IMAGE_ACTIVITY
                && resultCode == RESULT_OK) {
            Uri imageUri = null;
            if (data != null){
                imageUri = data.getData();
                selectedImagePath = getPath(imageUri);
                source = 1;
                Intent intent = new Intent(this, EditorActivity.class);
                startActivity(intent);
            }
        }

    }

    public String getPath(Uri uri) {
        if( uri == null ) {

            return null;
        }
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return uri.getPath();
    }

    private void openInfo() {
        new AlertDialog.Builder(this)
                .setTitle("Information")
                .setMessage("Create awesome memes with Memer, using most popular meme templates or your own images.\n\nMemer\n"
                        + "version 1.0\nAuthor: Aatos Lang")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int ok) {
                    //continue
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
}