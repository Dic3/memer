package com.example.aatoslocal.actionbartest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

// Handled the intro screen

public class SplashScreen extends Activity {
    private static final int SPLASH_DISPLAY_TIME = 2000;
    private static ImageView introLogo;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        introLogo = (ImageView)findViewById(R.id.logo_intro);
        introLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.intro_logo));

        new Handler().postDelayed(new Runnable() {
            public void run() {

                Intent intent = new Intent();
                intent.setClass(SplashScreen.this, MainActivity.class);

                SplashScreen.this.startActivity(intent);
                SplashScreen.this.finish();

                overridePendingTransition(R.anim.activityfadein,
                        R.anim.splashfadeout);

            }
        }, SPLASH_DISPLAY_TIME);
    }
}
